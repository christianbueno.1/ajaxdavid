from flask import Flask
import os
import yaml

def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    if app.config["ENV"] == "development" :
        dev_config_file = os.path.join(app.instance_path, 'development2.yaml')
        app.config.from_file(dev_config_file, load=yaml.safe_load)

    print(f'app.config["SQLALCHEMY_DATABASE_URI"] = {app.config["SQLALCHEMY_DATABASE_URI"]}')
    print(f'app.instance_path: {app.instance_path}')

    return app
